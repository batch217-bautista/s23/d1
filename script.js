console.log("Hello World");

// [SECTION] Objects
// an object is a data type that is used to represent real world objects
// Is a collection of related data and/or functionalities

/*
Syntax:
    let objectName = {
        keyA: valueA, 
        keyB: valueB
    }

*/

let cellphone = {
    name: "Nokia 3210",
    manufacturedDate: 1999,
}

console.log("Result from creating objects using initializer or literal notation"); //initializers and literal notation = {}
console.log(cellphone);
console.log(typeof cellphone);

// "this" inside an object
function Laptop(name, manufacturedDate) {
    this.name = name;
    this.manufacturedDate = manufacturedDate;
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using object constractors"); //object constructors --> "this" and "new"
console.log(laptop);

// Re-using Laptop function
let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using object constractors");
console.log(myLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating objects using object constractors");
console.log(oldLaptop); //undefined because "new" was not used

// Creating empty object
let computer = {}
let myComputer = new Object();

// [SECTION] Accessing object properties
// DOT NOTATION --> to access particular property in an object.

console.log("Result from dot notation: " + myLaptop.name);

// Square bracket notation
console.log("Result from square bracket notation: " + myLaptop['name']);

// Accessing array objects

let array = [laptop, myLaptop];

// Accessing array using indexes
console.log(array[0]['name']);
console.log(array[0].name);

// [SECTION] initializing, adding, deleting, re-assigning object properties

let car = {};

// Hoisting --> Reading a property ahead of its initialization
console.log(car);

car.name = "Honda Civic";
console.log("Result from square adding a property using dot notation:");
console.log(car);

// car.manufacturedDate = 2019;
// console.log("Result from square adding a property using dot notation:");
// console.log(car);

// Initializing or Adding property using bracket notation

car['manufacturedDate'] = 2019;
console.log(car.manufacturedDate);
console.log("Result from adding property using square bracket notation:");
console.log(car);

// delete object property
// delete car['manufacturedDate'];
delete car.manufacturedDate;
console.log("Result from deleting properties:");
console.log(car);

// Re-assigning object properties --> modifying/updating
car.name = "Dodge Charger R/T";
console.log("Result from re-assigning properties:");
console.log(car);

// [SECTION] Object Methods
// is a function which is a property of an object

let person = {
    name: "Shawn",
    talk: function(){
        console.log("Hello my name is " + this.name);
    }
}
console.log(person);
console.log("Result from object method:");
person.talk(); // calling/invoking a function

// Add a property with an object method/function
person.walk = function() {
    console.log(this.name + " walked 25 steps forward");
}
person.walk();

// Another example
// Nested properties inside an object
// Nested array within an object property
let friend = {
    firstName: "Samael",
    lastName: "DCLXVI",
    address: {
        city: "Austin",
        country: "Texas"
    },
    emails: ["samael@protonmail.com", "samael@mailfence.com"],
    introduce: function() {
        console.log("Hello my name is " + this.firstName + " " + this.lastName);
    }
}
friend.introduce();

// [SECTION] Real world application of objects

let myPokemon = {
    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log("This pokemon tackled target Pokemon");
        console.log("target's Pokemon's health is now reduced to _targetPokemonHealth_")
    },
    faint: function(){
        console.log("pokemon fainted");
    }
}

console.log(myPokemon);

// Creating an object constructor to help us on the process "this" and "new"

function Pokemon(name, level) {
    // properties
    this.name = name;
    this.level = level;
    this.health = 2 * level; // output: 32
    this.attack = level; // output: 3

    // Methods/Function
    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        console.log("targetPokemon's is now to _targetPokemonHealth_");
    
    this.faint = function(){
            console.log(this.name + "fainted");
        }
    }
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);